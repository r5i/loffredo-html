
function myMap() {
  var mapLat = parseFloat($('#closestLat').text());
  var mapLon = parseFloat($('#closestLon').text());
  var myLatLng = {lat: mapLat, lng: mapLon};
  var mapCanvas = document.getElementById("locationMap");
  var mapOptions = {
    center: myLatLng,
    zoom: 9
  }
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var image = 'presents/tyndall/designs/tyndall/default/images/branch_470_470.png';
  var marker = new google.maps.Marker({
      position:myLatLng,
      icon: image
  });
  marker.setMap(map);
}
</script>

<script>
    // Get User's Coordinate from their Browser
window.onload = function() {
    var cState = document.cookie.replace(/(?:(?:^|.*;\s*)region\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    var cCity = document.cookie.replace(/(?:(?:^|.*;\s*)city\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    var cLat = document.cookie.replace(/(?:(?:^|.*;\s*)cLat\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    var cLng = document.cookie.replace(/(?:(?:^|.*;\s*)cLng\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    var cZip = document.cookie.replace(/(?:(?:^|.*;\s*)zipCode\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    $('#city').text('Set Location');
    $('#city2').text('Set Location');
    $('#city3').text('Set Location');
    $('#comma').text('');
    if(cCity==""){
        $.getJSON('https://freegeoip.net/json/')
        .done (function(location)
        {
            $('#region_code').html(location.region_code);
            document.cookie = "region="+$("#region_code").html()+"; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
            $('#city').html(location.city);
            document.cookie = "city="+$("#city").html()+"; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
            $('#region_code2').html(location.region_code);
            $('#city2').html(location.city);
            $('#latitude').html(location.latitude);
            document.cookie = "cLat="+$('#latitude').html()+"; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
            $('#longitude').html(location.longitude);
            document.cookie = "cLng="+$('#longitude').html()+"; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
            $('#zipCode').html(location.zip_code);
            document.cookie = "zipCode="+$('#zipCode').html()+"; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
            $('#comma').text(',');
        });
        setTimeout(function(){
          if(cCity==''){
            geoFindMe();
          }
        }, 2000);
              
    }else{
        $('#city').text(cCity);
        $('#city2').text(cCity);
        $('#city3').text(cCity);
        $('#city4').text(cCity);
        $('#city5').text(cCity);
        $('#region_code').text(cState);
        $('#region_code2').text(cState);
        $('#region_code3').text(cState);
        $('#region_code4').text(cState);
        $('#region_code5').text(cState);
        $('#comma').text(',');
    }
    var cLat = document.cookie.replace(/(?:(?:^|.*;\s*)cLat\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    var cLng = document.cookie.replace(/(?:(?:^|.*;\s*)cLng\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    NearestCity(cLat, cLng);
}

function geoFindMe() {
  var output = document.getElementById("out");

  if (!navigator.geolocation){
    alert("Geolocation is not supported by your browser");
  }

  function success(position) {
    var latitude  = position.coords.latitude;
    var longitude = position.coords.longitude;

    //make a request to the google geocode api
    $.getJSON('http://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&sensor=true').done(function(response){
        //find the city and state
        var address_components = response.results[0].address_components;
        $.each(address_components, function(index, component){
          var types = component.types;
          $.each(types, function(index, type){
            if(type == 'locality') {
              city = component.long_name;
            }
            if(type == 'administrative_area_level_1') {
              state = component.short_name;
            }
            if(type == 'postal_code'){
              zip = component.short_name;
            }
          });
        });

        //pre-fill the city and state
        $('#city').html(city);
        document.cookie = "city="+city+"; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
        $('#region_code').html(state);
        document.cookie = "region="+state+"; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
        $('#city').html(city);
        $('#region_code2').html(state);
        $('#city2').html(city);
        $('#region_code3').html(state);
        $('#city3').html(city);
        $('#region_code4').html(state);
        $('#city4').html(city);
        $('#region_code').html(state);
        $('#city5').html(city);
        $('#region_code5').html(state);
        $('#zipCode4').text(zip);
        document.cookie = "zipCode="+zip+"; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
        document.cookie = "cLat="+latitude+"; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
        document.cookie = "cLng="+longitude+"; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";

        var geocoder =  new google.maps.Geocoder();
        geocoder.geocode( { 'address': city+','+state}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            var myLat = latitude;
            document.cookie = "cLat="+myLat+"; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
            var myLng = longitude; 
            document.cookie = "cLng="+myLng+"; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
          }
        });
    });
    
    NearestCity(latitude, longitude);
    myMap();
  }

  function error() {
    if (error.code == error.PERMISSION_DENIED){
        
    }
  }
  navigator.geolocation.getCurrentPosition(success, error);
}

// Callback function for asynchronous call to HTML5 geolocation
function UserLocation(position) {
  NearestCity(position.coords.latitude, position.coords.longitude);
}
</script>