$(document).ready(function(){
  $('.date').click(function(){
    var date = $(this);
    var d = date.data('date');
    date.addClass('active');
    $('.date').not(date).removeClass('active');
    $('.show-date').text(d);
    var bod = 'd'+d;
    Nbod = bod.replace(/[^0-9a-zA-Z]/g, '');
    $('.body-text').children().not('.'+Nbod).fadeOut();
    $('.'+Nbod).fadeIn();
  });

  $('.next').click(function(){
    var date = $('.active').data('date');
    var count = $('.active').data('count');
    if(count<9){
      count++;
      var newDate = $('.date-list').find('[data-count='+count+']');
      var t = newDate.text();
      newDate.addClass('active');
      $('.date').not(newDate).removeClass('active');
      $('.show-date').text(t);
      var d = 'd'+t;
      Nbod = d.replace(/[^0-9a-zA-Z]/g, '').toLowerCase();
      $('.body-text').children().not('.'+Nbod).fadeOut();
      $('.'+Nbod).fadeIn();
    }
  });
  $('.prev').click(function(){
    var date = $('.active').data('date');
    var count = $('.active').data('count');
    if(count>1){
      count--;
      var newDate = $('.date-list').find('[data-count='+count+']');
      var t = newDate.text();
      newDate.addClass('active');
      $('.date').not(newDate).removeClass('active');
      $('.show-date').text(t);
      var d = 'd'+t;
      Nbod = d.replace(/[^0-9a-zA-Z]/g, '').toLowerCase();
      $('.body-text').children().not('.'+Nbod).fadeOut();
      $('.'+Nbod).fadeIn();
    }
  });
});